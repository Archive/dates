/* -*- mode:C; c-file-style:"linux"; tab-width:8; -*- */
/* 
 *  DatesView - An electronic calendar widget optimised for embedded devices.
 *
 *  Principal author	: Chris Lord <chris@o-hand.com>
 *  Maemo port		: Tomas Frydrych <tf@o-hand.com>
 *
 *  Copyright (c) 2005 - 2006 OpenedHand Ltd - http://o-hand.com
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */

#ifndef __DATES_VIEW_H__
#define __DATES_VIEW_H__


#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <libecal/e-cal.h>
#include <libical/icaltime.h>
#include <libical/icalperiod.h>


G_BEGIN_DECLS

#define DATES_TYPE_VIEW		  (dates_view_get_type ())
#define DATES_VIEW(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), DATES_TYPE_VIEW, DatesView))
#define DATES_VIEW_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass), DATES_TYPE_VIEW, DatesViewClass))
#define DATES_IS_VIEW(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), DATES_TYPE_VIEW))
#define DATES_IS_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), DATES_TYPE_VIEW))
#define DATES_VIEW_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), DATES_TYPE_VIEW, DatesViewClass))

typedef struct _DatesView       DatesView;
typedef struct _DatesViewClass  DatesViewClass;

struct _DatesView
{
	GtkTable parent;
};

struct _DatesViewClass
{
  GtkTableClass parent_class;

  void (* date_changed)		(DatesView *view);
  void (* event_selected)	(DatesView *view);
  void (* event_moved)		(DatesView *view, ECalComponent *comp);
  void (* event_sized)		(DatesView *view, ECalComponent *comp);
  void (* event_activated)	(DatesView *view);
  void (* commit_event)		(DatesView *view);
  void (* ical_drop)		(DatesView *view, const gchar *vcard);
  void (*set_scroll_adjustments)(DatesView *view, GtkAdjustment *hadjustment,
		 		 GtkAdjustment *vadjustment);
};

GType		dates_view_get_type		(void) G_GNUC_CONST;
GtkWidget *	dates_view_new			();
void		dates_view_add_calendar		(DatesView *view, ECal *cal);
void		dates_view_remove_calendar	(DatesView *view, ECal *cal);
void		dates_view_remove_all_calendars	(DatesView *view);
gboolean	dates_view_get_selected_period	(DatesView *view,
						 struct icalperiodtype *period);
gboolean	dates_view_set_selected_event	(DatesView *view,
						 const gchar *uri_uid,
						 const gchar *rid);
ECalComponent *	dates_view_get_selected_event	(DatesView *view);
ECal *		dates_view_get_selected_event_cal (DatesView *view);
void		dates_view_set_read_only	(DatesView *view,
						 gboolean read_only);
void		dates_view_set_use_dragbox	(DatesView *view,
						 gboolean enable);
void		dates_view_set_single_click	(DatesView *view,
						 gboolean enable);
void		dates_view_set_snap		(DatesView *view, guint snap);
void		dates_view_set_week_start	(DatesView *view, guint day);
void 		dates_view_set_months_in_row 	(DatesView *view, guint months);
void 		dates_view_set_visible_months 	(DatesView *view, guint months);
void 		dates_view_set_visible_days 	(DatesView *view, guint days);
void 		dates_view_set_visible_hours 	(DatesView *view, guint hours);
void 		dates_view_set_date 		(DatesView *view,
						 icaltimetype *date);
void		dates_view_set_use_list		(DatesView *view,
						 gboolean use_list);
void		dates_view_set_use_24h		(DatesView *view,
						 gboolean use_24h);

gboolean	dates_view_get_read_only	(DatesView *view);
gboolean	dates_view_get_use_dragbox	(DatesView *view);
gboolean	dates_view_get_single_click	(DatesView *view);
guint		dates_view_get_snap		(DatesView *view);
guint		dates_view_get_week_start	(DatesView *view);
guint 		dates_view_get_months_in_row 	(DatesView *view);
guint 		dates_view_get_visible_months 	(DatesView *view);
guint 		dates_view_get_visible_days 	(DatesView *view);
guint 		dates_view_get_visible_hours 	(DatesView *view);
const icaltimetype *dates_view_get_date 	(DatesView *view);
gboolean	dates_view_get_use_list		(DatesView *view);
gboolean	dates_view_get_use_24h		(DatesView *view);
void            dates_view_get_visible_span     (DatesView *view,
						 struct icaltimetype *start,
						 struct icaltimetype *end);
icaltimezone * dates_view_get_zone(DatesView *view);


G_END_DECLS

#endif
