/* -*- mode:C; tab-width:4; -*- */
/* 
 *  Author: Tomas Frydrych <tf@o-hand.com>
 *
 *  Copyright (c) 2005 - 2006 OpenedHand Ltd - http://o-hand.com
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */

#ifndef DATES_TYPES_H
#define DATES_TYPES_H

#include <gtk/gtk.h>

#ifdef WITH_HILDON
#include <libosso.h>
#include <hildon/hildon-program.h>
#endif

#include "dates_view.h"

#define CALENDAR_GCONF_PREFIX   "/apps/evolution/calendar"
#define CALENDAR_GCONF_SOURCES CALENDAR_GCONF_PREFIX "/sources"
#define CALENDAR_GCONF_SELECTED CALENDAR_GCONF_PREFIX \
	"/display/selected_calendars"
#define CALENDAR_GCONF_PRIMARY CALENDAR_GCONF_PREFIX "/display/primary_calendar"
#define DATES_GCONF_PREFIX	"/apps/dates"
#define DATES_GCONF_WINDOW	"/apps/dates_window"
#define DATES_GCONF_ZOOM	DATES_GCONF_PREFIX "/zoom"

#define DEBUG 1

enum {
	COL_CALNAME,
	COL_CALPTR,
	COL_NOTREADONLY,
	COL_SELECTED,
	COL_LAST
};

typedef enum {
	NONE = 0,
	PENDING_CREATE,
	PENDING_DELETE,
	PENDING_CHANGE,
	PENDING_CAL_CHANGE,
} DatesPending;

typedef enum {
	NORMAL,
	NEW_EVENT,
} DatesEventType;

enum {
	DAY = 1,
	WEEK,
	FORTNIGHT,
	MONTH,
	YEAR,
	OTHER
};

#ifdef DEBUG
typedef enum {
	DATES_DEBUG_HIBERNATE	= 1 << 0,
	DATES_DEBUG_EDIT	= 1 << 1,
	DATES_DEBUG_XEMBED	= 1 << 2,
	DATES_DEBUG_CALENDAR	= 1 << 3,
	DATES_DEBUG_HILDON	= 1 << 4
} DatesDebugFlag;

static const GDebugKey dates_debug_keys[] = {
	{ "hibernate", DATES_DEBUG_HIBERNATE },
	{ "edit", DATES_DEBUG_EDIT },
	{ "xembed", DATES_DEBUG_XEMBED },
	{ "calendar", DATES_DEBUG_CALENDAR },
};
#endif


typedef struct {
	DatesView *view;
	guint zoom;
	ECalComponent *comp;
	ECal *cal;
	gboolean read_only;
	gboolean first_load;
	gboolean cal_loaded;
	GtkListStore *cal_list_store;
	gchar *uri_uid;
	GList *widgets;
	DatesPending waiting;
	DatesEventType event_type;
	ECal *dcal;	/* Default calendar */

	ESourceList *source_list; /* The sources for all the calendars */

	GtkWidget * TBNew;
	GtkWidget * TBEdit;
	GtkWidget * TBDelete;
	GtkWidget * TBBack;
	GtkWidget * TBToday;
	GtkWidget * TBForward;
	GtkWidget * TBZoomIn;
	GtkWidget * TBZoomOut;
	
#ifdef WITH_HILDON
	HildonProgram  *program;
	osso_context_t *osso_context;
	gboolean        init_done;
	gboolean        restored;
#endif
	
#ifdef DEBUG
	guint debug;
#endif
	/* widget list */
	GtkWidget * main_window;
    GtkWidget * main_menu;
	GtkWidget * MIZoomIn;
	GtkWidget * MIZoomOut;
	GtkWidget * header_label;
	GtkWidget * header_eventbox;
	GtkWidget * details_summary_entry;
	GtkWidget * details_start_label;
	GtkWidget * details_end_label;
	GtkWidget * details_dialog;
	GtkWidget * details_calendar_combobox;
	GtkWidget * details_textview;
	GtkWidget * details_delete_button;
	GtkWidget * details_notebook;
	GtkWidget * time_dialog;
	GtkWidget * repeats_dialog;
	GtkWidget * time_calendar;
	GtkWidget * time_forever_checkbutton;
	GtkWidget * time_table;
	GtkWidget * hour_entry;
	GtkWidget * lminute_entry;
	GtkWidget * rminute_entry;
	GtkWidget * cal_tree_view;
	GtkWidget * calendars_dialog;
	GtkWidget * exceptions_dialog;

#ifdef WITH_HILDON
	GtkWidget * time_date_editor;
	GtkWidget * time_time_editor;
#endif

	gchar *url_uid;
} DatesData;


#endif /* DATES_TYPES_H */
