/* -*- mode:C; tab-width:4; -*- */
/* 
 *  Author: Tomas Frydrych <tf@o-hand.com>
 *
 *  Copyright (c) 2005 - 2006 OpenedHand Ltd - http://o-hand.com
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */
#ifndef DATES_CALLBACKS_H
#define DATES_CALLBACKS_H

#include "dates_types.h"
#include "dates_view.h"

#include <gtk/gtk.h>


void
dates_back_cb (GtkButton *button, DatesData *data);

void
dates_forward_cb (GtkButton *button, DatesData *data);

void
dates_today_cb (GtkButton *button, DatesData *data);

/* FIXME -- this is not a callback */
void
dates_zoom_change (guint pos, DatesView *dates_view);

void
dates_zoom_in_cb (GtkButton *button, DatesData *data);

void
dates_zoom_out_cb (GtkButton *button, DatesData *data);

void
dates_date_changed_cb (DatesView *view, DatesData *d);

void
dates_about_cb (GtkWidget *widget, DatesData *d);

void
dates_event_selected_cb (DatesView *view, DatesData *d);

void
dates_commit_event_cb (GtkWidget *source, DatesData *data,
		       CalObjModType type);

void
dates_event_moved_cb (DatesView *view, ECalComponent *comp, DatesData *d);

void
dates_event_sized_cb (DatesView *view, ECalComponent *comp, DatesData *d);

void
dates_ical_drop_cb (DatesView *view, const gchar *ical, DatesData *d);

void
dates_details_time_entry_changed (GtkEditable *entry, gchar *new_text,
				  gint new_text_length,
				  gint *position,
				  DatesData *d);

gboolean
dates_details_time_entry_alt (GtkArrow *source, GtkWidget *entry,
			      DatesData *d, gint limit);

void
dates_details_time_hour_cb (GtkButton *source, DatesData *d);

void
dates_details_time_lminute_cb (GtkButton *source, DatesData *d);

void
dates_details_time_rminute_cb (GtkButton *source, DatesData *d);

#if 0
void
dates_repeats_cb (GtkWidget *source, DatesData *d);

void
dates_details_repeats_cb (GtkComboBox *combobox, DatesData *d);
#endif

void
dates_details_time_cb (DatesData *d, ECalComponentDateTime *time);

void
dates_details_update_time_label (DatesData *d, GtkWidget *label,
				 struct icaltimetype *time);

void
dates_details_time_start_cb (GtkWidget *source, DatesData *d);

void
dates_details_time_end_cb (GtkWidget *source, DatesData *d);

void
dates_edit_cb (GtkWidget *source, DatesData *d);

void
dates_details_ok_cb (GtkWidget *source, DatesData *d);

void
dates_delete_cb (GtkWidget *source, DatesData *d);

void
dates_details_cancel_cb (GtkWidget *source, DatesData *d);

gboolean
dates_details_close_cb (GtkWidget *widget, GdkEvent *event, DatesData *d);

gboolean
dates_select_event_idle_cb (gpointer data);

void
dates_new_cb (GtkWidget *source, DatesData *d);

void
dates_calendar_combo_changed_cb (GtkComboBox *widget, DatesData *d);

void
dates_cal_open_cb (ECal *ecal, ECalendarStatus status, DatesData *d);

void
dates_update_calendars (ESourceList *cal_list, DatesData *d);

void
dates_gconf_selected_cb (GConfClient *client, guint cnxn_id, GConfEntry *entry,
			 gpointer user_data);

void
dates_calendars_dialog_cb (GtkButton *button, DatesData *d);

void
dates_backend_died_cb (ECal *client, DatesData *d);

void
dates_sources_changed_cb (ESourceList *cal_list, DatesData *d);

void
dates_window_state_cb (GtkWidget *widget, GdkEventVisibility *event,
		       DatesData *d);

gboolean
dates_cal_key_press_cb (GtkWidget *widget, GdkEventKey *event, DatesData *d);

gboolean
dates_key_press_cb (GtkWidget *widget, GdkEventKey *event, DatesData *d);

gboolean
dates_button_press_cb (GtkWidget *widget, GdkEventButton *event, DatesData *d);

ECal *
dates_load_esource (ESource *esource, ECalSourceType  source_type,
					GSList *existing_clients, DatesData *d);

void
dates_autoselect_calendars (DatesData *d, ESourceList * cal_list);

void
dates_import_cb (GtkWidget *menu_item, gpointer user_data);

gboolean
dates_import_calendar_data_from_file (ECal *cal, gchar *filename,
		GError **error);


gboolean
dates_edit_event_on_startup (DatesData   *data,
                             const gchar *url_uri_in);

#endif /* DATES_CALLBACKS_H */
