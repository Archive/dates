/* -*- mode:C; tab-width:4; -*- */
/* 
 *  Author: Tomas Frydrych <tf@o-hand.com>
 *
 *  Copyright (c) 2005 - 2006 OpenedHand Ltd - http://o-hand.com
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */

#ifndef DATES_NAVIGATOR_H
#define DATES_NAVIGATOR_H

#include <gtk/gtktreemodelfilter.h>
#include <gtk/gtktreeview.h>

struct ECal;
struct GtkWidget;

#define DATES_TYPE_NAVIGATOR_MODEL	 (dates_navigator_model_get_type ())
#define DATES_NAVIGATOR_MODEL(obj)	 (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
			               DATES_TYPE_NAVIGATOR_MODEL, DatesNavigatorModel))
#define DATES_IS_NAVIGATOR_MODEL(obj)	 (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
					DATES_TYPE_NAVIGATOR_MODEL))

#define DATES_NAVIGATOR_MODEL_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass),\
                                             DATES_TYPE_NAVIGATOR_MODEL,      \
                                             DatesNavigatorModelClass))

#define DATES_IS_NAVIGATOR_MODEL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), \
                                             DATES_TYPE_NAVIGATOR_MODEL))

#define DATES_NAVIGATOR_MODEL_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj),\
                                             DATES_TYPE_NAVIGATOR_MODEL,   \
                                             DatesNavigatorModelClass))

typedef struct _DatesNavigatorModel        DatesNavigatorModel;
typedef struct _DatesNavigatorModelPrivate DatesNavigatorModelPrivate;
typedef struct _DatesNavigatorModelClass   DatesNavigatorModelClass;

struct _DatesNavigatorModel
{
    GtkTreeModelFilter       parent_instance;

    /* private bits -- no touching */
    DatesNavigatorModelPrivate  * priv;
};

struct _DatesNavigatorModelClass
{
    GtkTreeModelFilterClass parent_class;
};

/* Format to use in the When column */
typedef enum {
    DN_TimeOnly = 0, /* Print only time */
    DN_DateAndTime   /* Use both date and time */
} DNTimeFormat;


/* Enum for referring to the columns of our store */
typedef enum {
  DN_Name = 0,
  DN_Time,
  DN_Uid,
  DN_NameFolded,
  DN_n_columns
} DNColumns;

GType        dates_navigator_model_get_type        (void) G_GNUC_CONST;

GtkWidget *  dates_navigator_model_new             ();
void         dates_navigator_model_populate        (DatesNavigatorModel * nav,
													ECal * ecal,
													const gchar * query,
													DNTimeFormat format);


#define DATES_TYPE_NAVIGATOR_VIEW	   (dates_navigator_view_get_type ())
#define DATES_NAVIGATOR_VIEW(obj)	   (G_TYPE_CHECK_INSTANCE_CAST ((obj),\
					       DATES_TYPE_NAVIGATOR_VIEW, \
					       DatesNavigatorView))

#define DATES_IS_NAVIGATOR_VIEW(obj)	   (G_TYPE_CHECK_INSTANCE_TYPE ((obj),\
					       DATES_TYPE_NAVIGATOR_VIEW))

#define DATES_NAVIGATOR_VIEW_CLASS(klass)  (G_TYPE_CHECK_CLASS_CAST ((klass), \
                                             DATES_TYPE_NAVIGATOR_VIEW,       \
                                             DatesNavigatorViewClass))

#define DATES_IS_NAVIGATOR_VIEW_CLASS(klass)(G_TYPE_CHECK_CLASS_TYPE ((klass),\
                                             DATES_TYPE_NAVIGATOR_VIEW))

#define DATES_NAVIGATOR_VIEW_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj),\
                                             DATES_TYPE_NAVIGATOR_VIEW,       \
                                             DatesNavigatorViewClass))

typedef struct _DatesNavigatorView        DatesNavigatorView;
typedef struct _DatesNavigatorViewPrivate DatesNavigatorViewPrivate;
typedef struct _DatesNavigatorViewClass   DatesNavigatorViewClass;

struct _DatesNavigatorView
{
    GtkTreeView                  parent_instance;

    /* private bits -- no touching */
    DatesNavigatorViewPrivate  * priv;
};

struct _DatesNavigatorViewClass
{
    GtkTreeViewClass parent_class;
};

GtkWidget * dates_navigator_view_new ();

void        dates_navigator_view_populate (DatesNavigatorView * view,
										   ECal * ecal,
										   const gchar * query,
										   DNTimeFormat format);

void        dates_navigator_view_set_visible_func (DatesNavigatorView * n,
                                        GtkTreeModelFilterVisibleFunc func,
                                        gpointer data,
                                        GtkDestroyNotify destroy);

void        dates_navigator_view_refilter (DatesNavigatorView * view);


#define DATES_TYPE_NAVIGATOR	   (dates_navigator_get_type ())
#define DATES_NAVIGATOR(obj)	   (G_TYPE_CHECK_INSTANCE_CAST ((obj),\
					       DATES_TYPE_NAVIGATOR, \
					       DatesNavigator))

#define DATES_IS_NAVIGATOR(obj)	   (G_TYPE_CHECK_INSTANCE_TYPE ((obj),\
					       DATES_TYPE_NAVIGATOR))

#define DATES_NAVIGATOR_CLASS(klass)  (G_TYPE_CHECK_CLASS_CAST ((klass), \
                                             DATES_TYPE_NAVIGATOR,       \
                                             DatesNavigatorClass))

#define DATES_IS_NAVIGATOR_CLASS(klass)(G_TYPE_CHECK_CLASS_TYPE ((klass),\
                                             DATES_TYPE_NAVIGATOR))

#define DATES_NAVIGATOR_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj),\
                                             DATES_TYPE_NAVIGATOR,       \
                                             DatesNavigatorClass))

typedef struct _DatesNavigator        DatesNavigator;
typedef struct _DatesNavigatorPrivate DatesNavigatorPrivate;
typedef struct _DatesNavigatorClass   DatesNavigatorClass;

struct _DatesNavigator
{
    GtkVBox                 parent_instance;

    /* private bits -- no touching */
    DatesNavigatorPrivate  * priv;
};

struct _DatesNavigatorClass
{
    GtkVBoxClass parent_class;
};

GtkWidget * dates_navigator_new ();

void        dates_navigator_populate (DatesNavigator * nav, ECal * ecal,
									  const gchar *query, DNTimeFormat format);

void        dates_navigator_refilter (DatesNavigator * nav);

void        dates_navigator_set_search_mode (DatesNavigator * nav,
											 gboolean mode);

gboolean    dates_navigator_get_search_mode (DatesNavigator * nav);

void        dates_navigator_show_search_bar (DatesNavigator * nav,
											 gboolean show);
G_END_DECLS

#endif /* DATES_NAVIGATOR_H */
