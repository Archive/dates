/* -*- mode:C; tab-width:4; -*- */
/* 
 *  Author: Tomas Frydrych <tf@o-hand.com>
 *
 *  Copyright (c) 2005 - 2006 OpenedHand Ltd - http://o-hand.com
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */
#ifndef DATES_UI_GTK_H
#define DATES_UI_GTK_H

#include "dates_types.h"

#ifdef WITH_HILDON
#define DATES_MENU_WITHOUT_BAR 1
#endif

struct DatesData;

void dates_platform_init (DatesData * d);
void dates_platform_create_ui (DatesData * d);
void dates_platform_pre_show (DatesData *d);

void dates_platform_details_dlg (DatesData *d, gboolean show);
void dates_platform_time_dlg (DatesData *d, gboolean show);
void dates_platform_repeats_dlg (DatesData *d, gboolean show);
void dates_platform_calendars_dlg (DatesData *d, gboolean show);
void dates_platform_exceptions_dlg (DatesData *d, gboolean show);

void dates_platform_cal_open (DatesData *d);
void dates_platform_import_dialog (DatesData *d);

#endif /* DATES_UI_GTK_H */
