
#include <gtk/gtk.h>
#include "dates_view.h"

int
main (int argc, char **argv)
{
	GtkWidget *window, *dates_view;

	gtk_init (&argc, &argv);
	
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	dates_view = dates_view_new ();
	dates_view_set_visible_months (DATES_VIEW (dates_view), 12);
	
	gtk_container_add (GTK_CONTAINER (window), dates_view);
		
	g_signal_connect (G_OBJECT (window), "destroy",
			  G_CALLBACK (gtk_main_quit), NULL);
	
	gtk_widget_show (dates_view);
	gtk_widget_show (window);
	gtk_main ();
	
	return 0;
}
