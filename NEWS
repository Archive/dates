Dates 0.4.11
============

* Use nl_langinfo to get the locale-dependent first-day-of-week
* Add sl for Slovenian language
* Added Slovenian translation
* Updated Polish translation

Dates 0.4.10
============

* Detect the system timezone instead of relying on Evolution


Dates 0.4.9
===========

* Don't display dates before 1900
* Determine the local timezone without depending on Evolution
* Don't require that iCalendar files being imported are in UTF-8
* Truncate summary when confirming deletion
* Updated translations


Dates 0.4.8
===========

* Fix selection logic in year view
* Fix logic error in month calculation
* Handle date selection when week crosses months
* Use full day name in the window title
* Show an error when no events were imported

Dates 0.4.7
===========

* Fix about box (Marcin Juszkiewicz.)
* Correctly deal with GOption errors using gtk_init_with_args (Jonny Lamb.)
* Fix a bug where events in the first week of January are missing in the
  week view when the wek doesn't start on a Monday (Chris Lord.)

* New and updated translations:
        - Polish (pl) - Tomasz Dominikowski
        - Swedish (sv) - Daniel Nylander
        - Spanish (es) - Jorge Gonzalez
        - Brazilian Portuguese (pt_BR) - Og Maciel
        - German (de) - Mario Blättermann
        - Basque (eu) - Inaki Larranaga Murgoitio
        - British English (en_GB) - Jen Ockwell

Dates 0.4.6
===========

* More strings translated. (Stéphane Raimbault)
* Translation strings no longer have markup. (Thomas Perl)
* Correctly formatted application name appears in the about box. (Ross Burton)
* Uses the timezone when refreshing the time. (Thomas Perl)
* More Hildon porting. (Thomas Perl)
* Fixed crash when the calendar list is updated. (Laurent Bigonville)
* Covert locale strings to UTF-8 so that Pango can use them. (Georges Da Costa)

* New and updated tranlations:
          - French (fr) - Stéphane Raimbault
          - German (de) - Thomas Perl
          - Basque (eu) - Inko I. A.
          - Norwegian Bokmål (nb) - Espen Stefansen
          - Danish (da) - Kristian Poul Herkild

Dates 0.4.5
===========

* Translate import dialog title as well (#551, Loïc Minier).
* Add ellipsis to Import, as per HIG (#536, thanks Michael Monreal).
* Updated to latest Hildon (Rob Bradford)
* Memory leaks fixed. (Chris Lord)

Dates 0.4.4
===========

* Don't install the datesview library, this API is not really useful for 3rd
  parties to build against. (Ross Burton)
* Fix the 'toolbar' buttons so they are themeable correctly under Sato.
  (Rob Bradford)
* Fix an infinite loop bug when moving events backwards in January 
  (Chris Lord)
* Updated translation: Danish (da) - Kristian Poul Herkild

Dates 0.4.3
===========

* Fix problem with clipping in the top area containing the day names.
* Support for importing an ical file into a calendar.

Dates 0.4.2
===========

* Fix a problem where new local calendars cannot be created on Dates where
  Dates has been upgraded from an older version where the system calendar was
  setup incorrectly. 
* Add a full week view between working month and working week.

Dates 0.4.1
===========

* Fix crash when no calendar type is chosen in the new calendar dialog. Thanks
  to Pierpaolo Toniolo for spotting it.

Dates 0.4.0
===========

* Support for adding, modifying and deleting calendars.
* Support for remote calendars.
* Improved rendering, including:
        - Clearer drawing of the the day number,
        - Drawing of events outside the current month/week when the days are
          visible,
        - Improved clipping of summary text to prevent partially drawn lines,
        - Clearer drawing when dragging events
* New translations:
        - Polish (po) - Tomasz Dominikowski
        - Swedish (sv) - Daniel Nylander
        - Danish (da) - Kristian Poul Herkild
        - Norwegian Bokmal (nb) - Espen Stefansen
        - Catalan (ca) - Gil Forcada
        - Finnish (fi) - Kalle Vahlman
        - Italian (it) - Emmanuele Bassi

+ Many more small improvements and fixes

Dates 0.3.1
===========

* Fix unselectable days in December day/week/month views
* Update French translation (Olivier Le Thanh Duong)


Dates 0.3
=========

* Select all calendars if none are selected
* Store window location in the right GConf paths
* Add man page (Kestutis Biliunas)
