#!/bin/bash

if [ $# != 1 ]; then
  echo "requires one argument, a file with list of restored files"
  exit 1
fi

RESTORE_LIST=$1
if ! [ -e $RESTORE_LIST ]; then
  echo "no restore file found"
  exit 1
fi

if ! [ -d $HOME/.evolution/calendar ]; then
  echo "no $HOME/.evolution/calendar directory found"
  exit 1
fi

exec 2>/dev/null

for file in `find $HOME/.evolution/calendar`; do
  if ! grep "$file" $RESTORE_LIST; then
    rm -f $file
  fi
done

exit 0
