# Slovak translation for dates.
# Copyright (C) 2010, 2011 Free Software Foundation, Inc.
# This file is distributed under the same license as the dates package.
# Robert Hartl <hartl.robert@gmail.com>, 2010, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: dates\n"
"Report-Msgid-Bugs-To: http://bugzilla.openedhand.com/enter_bug.cgi?"
"product=Dates&keywords=I18N+L10N&component=General\n"
"POT-Creation-Date: 2011-03-19 02:29+0000\n"
"PO-Revision-Date: 2011-04-26 14:59+0200\n"
"Last-Translator: Robert Hartl <hartl.robert@gmail.com>\n"
"Language-Team: Slovak <gnome-sk-list@gnome.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../data/dates.desktop.in.h:1 ../src/dates_callbacks.c:316
#: ../src/dates_hildon.c:402 ../src/dates_hildon.c:514
#: ../src/dates_hildon.c:524 ../src/dates_gtk.c:54 ../src/dates_gtk.c:72
#: ../src/dates_main.c:405
msgid "Dates"
msgstr "Termíny"

#: ../data/dates.desktop.in.h:2
msgid "Your appointments"
msgstr "Vaše schôdzky"

#: ../src/dates_callbacks.c:347
msgid ""
"This program is free software; you can redistribute it and/or modify\n"
"it under the terms of the GNU General Public License as published by\n"
"the Free Software Foundation; either version 2, or (at your option)\n"
"any later version.\n"
"\n"
"This program is distributed in the hope that it will be useful,\n"
"but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
"GNU General Public License for more details."
msgstr ""
"Tento program je slobodný softvér; môžete ho ďalej šíriť a\n"
"upravovať podľa ustanovení licencie GNU General Public Licence\n"
"(Všeobecná zverejňovacia licencia GNU), vydávanej nadáciou\n"
"Free Software Foundation a to buď podľa 2. verzie tejto Licencie,\n"
" alebo (podľa vášho uváženia) ktorejkoľvek neskoršej verzie. \n"
"\n"
"Tento program je rozširovaný v nádeji, že bude užitočný,\n"
"avšak BEZ AKEJKOĽVEK ZÁRUKY. Neposkytujú sa ani odvodené záruky\n"
"PREDAJNOSTI alebo VHODNOSTI NA URČITÝ ÚČEL. Ďalšie podrobnosti\n"
"hľadajte v licencii GNU General Public License. "

#. Translators: please translate this as your own name and optionally email
#. like so: "Your Name <your@email.com>"
#: ../src/dates_callbacks.c:359
msgid "translator-credits"
msgstr "Robert Hartl <hartl.robert@gmail.com>"

#: ../src/dates_callbacks.c:1116
msgid "Unknown event"
msgstr "Neznáma udalosť"

#: ../src/dates_callbacks.c:1122 ../src/dates_callbacks.c:1136
#, c-format
msgid "Are you sure you want to delete event '%s'?"
msgstr "Naozaj chcete odstrániť udalosť „%s“?"

#: ../src/dates_callbacks.c:1126 ../src/dates_callbacks.c:1139
msgid "Keep event"
msgstr "Zachovať udalosť"

#: ../src/dates_callbacks.c:1127 ../src/dates_callbacks.c:1140
msgid "Delete event"
msgstr "Odstrániť udalosť"

#: ../src/dates_callbacks.c:1268
msgid "New event"
msgstr "Nová udalosť"

#: ../src/dates_callbacks.c:1349
msgid ""
"Dates failed to open the system calendar. You will not be able to view or "
"create events; this may be remedied by restarting."
msgstr ""
"Aplikácii Termíny sa nepodarilo otvoriť systémový kalendár. Nebudete môcť "
"prehliadať a vytvárať udalosti. Problém možno vyrieši reštart."

#: ../src/dates_callbacks.c:1440 ../src/dates_callbacks.c:1444
msgid " (Read Only)"
msgstr " (iba na čítanie)"

# neznámy kalendár...
#: ../src/dates_callbacks.c:1442 ../src/dates_callbacks.c:1444
msgid "Unknown"
msgstr "Neznámy"

#: ../src/dates_callbacks.c:1906
msgid "No calendar events found."
msgstr "V kalendári nebola nájdená žiadna udalosť."

#: ../src/dates_hildon.c:448 ../src/dates_gtk.c:516
msgid "Time"
msgstr "Čas"

#: ../src/dates_hildon.c:475 ../src/dates_gtk.c:377 ../src/dates_gtk.c:649
msgid "Forever"
msgstr "Navždy"

#: ../src/dates_gtk.c:99
msgid "Find:"
msgstr "Hľadať:"

#: ../src/dates_gtk.c:169 ../src/dates_gtk.c:317
msgid "Details"
msgstr "Podrobnosti"

#: ../src/dates_gtk.c:197
msgid "Calendar:"
msgstr "Kalendár:"

#: ../src/dates_gtk.c:215
msgid "Summary:"
msgstr "Súhrn:"

#: ../src/dates_gtk.c:231
msgid "Time:"
msgstr "Čas:"

#: ../src/dates_gtk.c:252
msgid "Start"
msgstr "Začiatok"

# použité pri nastavovaní trvania udalosti
#: ../src/dates_gtk.c:261
msgid "to"
msgstr "do"

#: ../src/dates_gtk.c:269
msgid "End"
msgstr "Koniec"

#: ../src/dates_gtk.c:277
msgid "Summary"
msgstr "Súhrn"

#: ../src/dates_gtk.c:292
msgid "Details:"
msgstr "Detaily:"

#: ../src/dates_gtk.c:332
msgid "Repeats:"
msgstr "Opakovania:"

# vzťahuje sa na opakovanie
#: ../src/dates_gtk.c:346
msgid "None"
msgstr "Žiadne"

#: ../src/dates_gtk.c:347
msgid "Every Day"
msgstr "Každý deň"

#: ../src/dates_gtk.c:349
msgid "Every Week"
msgstr "Každý týždeň"

#: ../src/dates_gtk.c:351
msgid "Every Fortnight"
msgstr "Každých štrnásť dní"

#: ../src/dates_gtk.c:353
msgid "Every Month"
msgstr "Každý mesiac"

#: ../src/dates_gtk.c:355
msgid "Every Year"
msgstr "Každý rok"

#: ../src/dates_gtk.c:357
msgid "Custom"
msgstr "Vlastné"

#: ../src/dates_gtk.c:359
msgid "On:"
msgstr "Od:"

#: ../src/dates_gtk.c:365
msgid "Until:"
msgstr "Do:"

#: ../src/dates_gtk.c:386
msgid "Sun"
msgstr "Ne"

#: ../src/dates_gtk.c:391
msgid "Mon"
msgstr "Po"

#: ../src/dates_gtk.c:396
msgid "Tue"
msgstr "Ut"

#: ../src/dates_gtk.c:401
msgid "Wed"
msgstr "St"

#: ../src/dates_gtk.c:406
msgid "Thu"
msgstr "Št"

#: ../src/dates_gtk.c:411
msgid "Fri"
msgstr "Pi"

#: ../src/dates_gtk.c:416
msgid "Sat"
msgstr "So"

#: ../src/dates_gtk.c:444
msgid "_Exceptions"
msgstr "_Výnimky"

#: ../src/dates_gtk.c:448 ../src/dates_gtk.c:1687
msgid "Repeats"
msgstr "Opakovania"

#: ../src/dates_gtk.c:554 ../src/dates_view.c:1331
msgid "AM"
msgstr "AM"

#. result for the dialog
#. Create the dialog
#: ../src/dates_gtk.c:788
msgid "New calendar"
msgstr "Nový kalendár"

#. Create the label
#: ../src/dates_gtk.c:808 ../src/dates_gtk.c:1221 ../src/dates_main.c:487
msgid "Calendar"
msgstr "Kalendár"

#. Create the label for the type
#: ../src/dates_gtk.c:845 ../src/dates_gtk.c:1258
msgid "Type:"
msgstr "Typ:"

# používa sa pri názve kalendára
#. Create the label for the name
#: ../src/dates_gtk.c:906 ../src/dates_gtk.c:1291
msgid "_Name:"
msgstr "_Názov:"

#. Create the label for the name
#: ../src/dates_gtk.c:940 ../src/dates_gtk.c:1332
msgid "_Colour:"
msgstr "_Farba:"

# jedná sa o umiestnenie suboru kalendára na webe
#. Create the label for the uri
#: ../src/dates_gtk.c:979 ../src/dates_gtk.c:1388
msgid "_Location:"
msgstr "_Umiestnenie:"

#: ../src/dates_gtk.c:1117
#, c-format
msgid "Are you sure you want to delete the calendar named '%s'?"
msgstr "Naozaj chcete odstrániť kalendár nazvaný „%s“?"

#. Create the dialog
#: ../src/dates_gtk.c:1190 ../src/dates_gtk.c:1198
msgid "Edit calendar"
msgstr "Upraviť kalendár"

#: ../src/dates_gtk.c:1601
msgid "Calendars"
msgstr "Kalendáre"

#: ../src/dates_gtk.c:1712
msgid ""
"<i>You can toggle a repeat\n"
"by double-clicking a date\n"
"on the calendar.</i>"
msgstr ""
"<i>Opakovanie môžete\n"
"prepnúť dvojitým kliknutím\n"
"na dátum v kalendári.</i>"

#: ../src/dates_gtk.c:1775
msgid "_Today"
msgstr "_Dnes"

#: ../src/dates_gtk.c:1868
msgid "_Calendar"
msgstr "_Kalendár"

#: ../src/dates_gtk.c:1883
msgid "_Import..."
msgstr "_Import..."

#. Edit menu commented out. Might be useful later...
#.
#. menuitem2 = gtk_menu_item_new_with_mnemonic (_("_Edit"));
#. gtk_widget_show (menuitem2);
#. #ifndef DATES_MENU_WITHOUT_BAR
#. gtk_container_add (GTK_CONTAINER (d->main_menu), menuitem2);
#. #else
#. gtk_menu_shell_append (GTK_MENU_SHELL (d->main_menu), menuitem2);
#. #endif
#.
#. menuitem2_menu = gtk_menu_new ();
#. gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem2), menuitem2_menu);
#.
#. cut_menuitem = gtk_image_menu_item_new_from_stock ("gtk-cut",
#. *accel_group);
#. gtk_widget_show (cut_menuitem);
#. gtk_container_add (GTK_CONTAINER (menuitem2_menu), cut_menuitem);
#. gtk_widget_set_sensitive (cut_menuitem, FALSE);
#.
#. copy_menuitem = gtk_image_menu_item_new_from_stock ("gtk-copy",
#. *accel_group);
#. gtk_widget_show (copy_menuitem);
#. gtk_container_add (GTK_CONTAINER (menuitem2_menu), copy_menuitem);
#. gtk_widget_set_sensitive (copy_menuitem, FALSE);
#.
#. paste_menuitem = gtk_image_menu_item_new_from_stock ("gtk-paste",
#. *accel_group);
#. gtk_widget_show (paste_menuitem);
#. gtk_container_add (GTK_CONTAINER (menuitem2_menu), paste_menuitem);
#. gtk_widget_set_sensitive (paste_menuitem, FALSE);
#.
#: ../src/dates_gtk.c:1929
msgid "_View"
msgstr "_Zobraziť"

#: ../src/dates_gtk.c:1960
msgid "_Help"
msgstr "_Pomocník"

#.
#. * Hack an hbox into the top of the GtkFileChooserDialog. Done this way
#. * rather than with a GtkFileChooserWidget to avoid some horrible faffing
#. * about with sizing.
#.
#: ../src/dates_gtk.c:2148
msgid "Import calendar"
msgstr "Import kalendára"

#: ../src/dates_gtk.c:2160
msgid "Target _calendar:"
msgstr "_Cieľový kalendár:"

#: ../src/dates_gtk.c:2238
#, c-format
msgid "Error when importing: %s"
msgstr "Chyba počas importu: %s"

#: ../src/dates_gtk.c:2257
msgid "No writable calendars to import into."
msgstr "Pre import nie je dostupný zapisovateľný kalendár."

#: ../src/dates_view.c:1331
msgid "PM"
msgstr "PM"

#: ../src/dates_main.c:105
msgid "On This Computer"
msgstr "Na tomto počítači"

# jedná sa o názov predvoleného kalendára
#. Now create a default source for it
#: ../src/dates_main.c:112
msgid "Personal"
msgstr "Osobný"

#: ../src/dates_main.c:150
msgid "On The Web"
msgstr "Na webe"

#: ../src/dates_main.c:408
msgid " - A light-weight, zooming calendar"
msgstr " - Jednoduchý kalendár s nastaviteľnou veľkosťou"

#: ../src/dates_main.c:481
msgid "Selected"
msgstr "Vybraný"

#: ../src/gconf-bridge.c:1218
#, c-format
msgid "GConf error: %s"
msgstr "Chyba systému GConf: %s"

#: ../src/gconf-bridge.c:1228
msgid "All further errors shown only on terminal."
msgstr "Všetky ostatné chyby sa zobrazujú iba v termináli."
